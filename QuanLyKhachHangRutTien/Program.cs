﻿using QuanLyKhachHangRutTien;
using System.Collections;
using System.Reflection.Metadata;

Console.OutputEncoding = System.Text.Encoding.UTF8;
QueueInBank queue = new QueueInBank(10);
Menu menu = new Menu();
int choice = 0;
queue.ReadQueueData();
do
{
    try
    {
        menu.QueueMenu();
        queue.DisplayQueue();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Customer customer = new Customer();
                queue.Input(customer);
                queue.AddCustomer(customer);
                queue.WriteQueueData();
                break;
            case 2:
                var nextCustomer = queue.CallNextCustomer();
                Console.WriteLine("Mời " + nextCustomer.customerName +" |Số:"+ nextCustomer.number + " |Rút " + nextCustomer.amountMoney + " vnd");
                queue.WriteQueueData();
                break;
            case 3:
                Console.WriteLine("Danh sách đã rút tiền:");
                Console.Write(queue.ListOfWithdrawn());
                Console.WriteLine("\n-------------------------");
                Console.WriteLine("Tổng số tiền đã rút: " + queue.TotalAmountWithdrawn());
                Console.WriteLine("-------------------------");
                Console.WriteLine("Số tiền còn lại trong hàng đợi " + queue.TotalRemainderInQueue());
                break;
            case 4:
                Console.WriteLine("Chúc bạn một ngày tốt lành !");
                Environment.Exit(0);

                break;
            default:
                throw new Exception("Vui lòng nhập số từ 1 đến 5");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
    finally
    {
        queue.WriteQueueData();
    }
} while (choice != 4);

