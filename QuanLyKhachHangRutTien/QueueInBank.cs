﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKhachHangRutTien
{
    public class QueueInBank
    {
        const int ONE_BILLION = 1_000_000_000;
        Customer[] customers;
        Customer[] dequeueList;
        int dequeueListPointer = -1;
        int maxQueue = 0;
        int top;
        int bot = -1;
        string filePath = @"Data.txt";
        public QueueInBank(int maxQueue)
        {
            this.maxQueue = maxQueue;
            customers = new Customer[maxQueue];
            dequeueList = new Customer[maxQueue];
            top = 0;
        }
        public void WriteQueueData()
        {
            if (!File.Exists(filePath))
            {
                File.Create(filePath);
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                for (int i = top; i <= bot; i++)
                {
                    sb.Append($"{customers[i].customerName + "-" + customers[i].amountMoney + "|"}").ToString();
                    File.WriteAllText(filePath, sb.ToString());
                }
            }
        }
        public void ReadQueueData()
        {
            if (!File.Exists(filePath))
            {
                File.Create(filePath);
            }
            else
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = reader.ReadLine()) is not null)
                    {
                        var customer = line.Split('|');
                        for (int i = 0; i < customer.Length; i++)
                        {
                            var getCustomer = customer[i];
                            var seperatorPosition = getCustomer.IndexOf('-');
                            if (seperatorPosition != -1)
                            {
                                var name = getCustomer.Substring(0, seperatorPosition);
                                var money = decimal.Parse(getCustomer.Substring(seperatorPosition + 1));
                                customers[i] = new Customer { customerName = name, amountMoney = money };
                                AddCustomer(customers[i]);
                            }
                        }
                    }
                }
            }
        }
        public bool IsVIP(Customer customer) => customer.amountMoney >= ONE_BILLION;
        public void Input(Customer customer)
        {
            Console.Write("Nhập tên: ");
            customer.customerName = Console.ReadLine();
            Console.Write("Số tiền cần rút: ");
            customer.amountMoney = decimal.Parse(Console.ReadLine());
        }
        public void AddCustomer(Customer customer)
        {
            if (bot == maxQueue - 1)
            {
                throw new Exception("Hàng đợi đầy");
            }
            bot++;
            customer.number = bot + 1;
            if (IsVIP(customer))
            {
                for (int i = bot; i > top; i--)
                {
                    customers[i] = customers[i - 1];
                }
                customers[top] = customer;
            }
            else
            {
                customers[bot] = customer;
            }
        }
        public Customer CallNextCustomer()
        {
            if (bot == -1)
            {
                throw new Exception("Hàng đợi trống");
            }
            dequeueListPointer++;
            var customer = customers[top];
            dequeueList[dequeueListPointer] = customers[top];
            if (top == bot)
            {
                top = 0;
                bot = -1;
            }
            else
            {
                top++;
            }
            return customer;
        }
        public string ViewThreeCustomer()
        {
            StringBuilder sb = new StringBuilder();
            int checkThreeCustomer = Math.Min(bot + 1, top + 3);
            for (int i = top; i < checkThreeCustomer; i++)
            {
                sb.Append("Số: " + customers[i].number + "-" + customers[i].customerName + " | ");
            }
            return sb.ToString();
        }
        public string ListOfWithdrawn()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= dequeueListPointer; i++)
            {
                sb.Append($"{dequeueList[i].customerName + " | "}");
            }
            return sb.ToString();
        }
        public decimal TotalAmountWithdrawn()
        {
            decimal sum = 0;
            for (int i = 0; i <= dequeueListPointer; i++)
            {
                sum += dequeueList[i].amountMoney;
            }
            return sum;
        }
        public decimal TotalRemainderInQueue()
        {
            decimal sum = 0;
            for (int i = top; i <= bot; i++)
            {
                sum += customers[i].amountMoney;
            }
            return sum;
        }
        public void DisplayQueue()
        {
            if (bot > -1)
            {
                Console.WriteLine("------------------------------");
                Console.Write(ViewThreeCustomer());
                Console.WriteLine("\n------------------------------");
            }
        }
    }
}
